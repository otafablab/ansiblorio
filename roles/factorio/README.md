Factorio
========

Provision a factorio server

- Creates the `factorio` user
- Downloads the latest factorio headless server to `/opt/factorio`
- Configures the server and mods
  - Currently the mods are copied from a temporary hetzner volume
- Copies a premade save from `files/saves/ansiblorio.zip` if the save
  doesn't exist on the server
- Sets up a systemd service which runs the server as the `factorio`
  user with tmux and rlwrap. The tmux session name is `factorio`
  - Connect to the tmux session as the `factorio` user with `tmux
    attach -t factorio`

Requirements
------------

None

Role Variables
--------------

| Name                           | Description                                                          | Default                                        |
|--------------------------------|----------------------------------------------------------------------|------------------------------------------------|
| `factorio_user_uid`            | Arbitrary UID and GID of the factorio user to be created on the host | 1337                                           |
| `factorio_name`                | Name of the server                                                   | Otafablab highly experminental factorio server |
| `factorio_description`         | Description of the server                                            | Very description                               |
| `factorio_enable_cache`        | Enable caching to `factorio_cache_directory`                         | yes                                            |
| `factorio_cache_directory`     | The directory to cache to                                            | /mnt/HC_Volume_20926607                        |
| `factorio_version`             | The game version                                                     |                                                |
| `vault_factorio_token`         | Authentication token                                                 |                                                |
| `vault_factorio_game_password` | Server password                                                      |                                                |


Dependencies
------------

None

License
-------

MIT

Author Information
------------------

<https://fablab.rip>
